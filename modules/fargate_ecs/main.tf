module "ecs_cluster" {
  source             = "terraform-aws-modules/ecs/aws"
  name               = var.ecs_name
  container_insights = true
}

data "aws_iam_policy" "AmazonECSTaskExecutionRolePolicy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

resource "aws_iam_role" "SecAppECSExecutionRole" {
  name = "SecAppECSExecutionRole"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs_execution_role_attachment" {
  role       = aws_iam_role.SecAppECSExecutionRole.name
  policy_arn = data.aws_iam_policy.AmazonECSTaskExecutionRolePolicy.arn
}

resource "aws_cloudwatch_log_group" "sec_app_containers" {
  name = "/aws/ecs/containerinsights/sec-app-ecs/logs"
}

resource "aws_security_group" "ecs_sg" {
  name   = "ecs"
  vpc_id = var.vpc_id

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  egress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    prefix_list_ids = [var.s3_endpoint_prefix_list_id]
  }

  ingress {
    from_port   = 0
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

}

resource "aws_ecs_task_definition" "task_definition" {
  family                   = "sample-fargate-app"
  container_definitions    = file("modules/fargate_ecs/task-definitions/service.json")
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  execution_role_arn       = aws_iam_role.SecAppECSExecutionRole.arn
  cpu                      = 512
  memory                   = 1024
}

resource "aws_ecs_service" "ecs_service" {
  name            = var.ecs_service_name
  cluster         = module.ecs_cluster.this_ecs_cluster_id
  task_definition = aws_ecs_task_definition.task_definition.arn
  desired_count   = 2
  launch_type     = "FARGATE"

  deployment_controller {
    type = "CODE_DEPLOY"
  }

  load_balancer {
    target_group_arn = var.ecs_alb_target_group[0]
    container_name   = "sec-app"
    container_port   = 80
  }

  network_configuration {
    security_groups = [aws_security_group.ecs_sg.id]
    subnets         = var.vpc_subnets
  }
}

resource "aws_iam_role" "AWSCodeDeployRoleForECS" {
  name               = "AWSCodeDeployRoleForECS"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "codedeploy.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy" "AWSCodeDeployRoleForECS" {
  arn = "arn:aws:iam::aws:policy/AWSCodeDeployRoleForECS"
}

resource "aws_iam_role_policy_attachment" "AWSCodeDeployRoleForECSAttach" {
  role       = aws_iam_role.AWSCodeDeployRoleForECS.name
  policy_arn = data.aws_iam_policy.AWSCodeDeployRoleForECS.arn
}

resource "aws_codedeploy_app" "sec-app" {
  compute_platform = "ECS"
  name             = "sec-app"
}

resource "aws_codedeploy_deployment_group" "sec-app" {
  app_name               = aws_codedeploy_app.sec-app.name
  deployment_config_name = "CodeDeployDefault.ECSAllAtOnce"
  deployment_group_name  = "sec-app-group"
  service_role_arn       = aws_iam_role.AWSCodeDeployRoleForECS.arn

  auto_rollback_configuration {
    enabled = true
    events  = ["DEPLOYMENT_FAILURE"]
  }

  blue_green_deployment_config {
    deployment_ready_option {
      action_on_timeout = "CONTINUE_DEPLOYMENT"
    }

    terminate_blue_instances_on_deployment_success {
      action                           = "TERMINATE"
      termination_wait_time_in_minutes = 5
    }
  }

  deployment_style {
    deployment_option = "WITH_TRAFFIC_CONTROL"
    deployment_type   = "BLUE_GREEN"
  }

  ecs_service {
    cluster_name = var.ecs_name
    service_name = aws_ecs_service.ecs_service.name
  }

  load_balancer_info {
    target_group_pair_info {
      prod_traffic_route {
        listener_arns = var.ecs_alb_prod_listeners
      }

      target_group {
        name = var.ecs_alb_target_group_names[0]
      }

      target_group {
        name = var.ecs_alb_target_group_names[1]
      }
    }
  }
}

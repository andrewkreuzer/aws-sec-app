variable "ecs_name" {
  description = "Name of ecs"
  type        = string
  default     = "sec-app-ecs"
}

variable "ecs_tags" {
  description = "Tags for ecs containers"
  type        = map(string)
  default = {
    Terraform   = "true"
    Environment = "dev"
  }
}

variable "ecs_service_name" {
  description = "Name of ecs service"
  type        = string
  default     = "sec-app-ecs-service"
}

variable "vpc_id" {
  description = "Id for vpc"
  type        = string
}

variable "vpc_cidr_block" {
  description = "CIDR block for vpc"
  type        = string
}

variable "vpc_subnets" {
  description = "Subnets for ecs services from vpc"
  type        = list(string)
}

variable "s3_endpoint_prefix_list_id" {
  description = "prefix list id for s3 endpoint"
  type        = string
}

variable "ecs_alb_target_group" {
  description = "Application load balancer"
  type        = list(string)
}

variable "ecs_alb_target_group_names" {
  description = "Application load balancer names"
  type        = list(string)
}

variable "ecs_alb_prod_listeners" {
  description = "Application load balancer listener"
  type        = list(string)
}

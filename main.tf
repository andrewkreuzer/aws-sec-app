terraform {
  backend "s3" {
    bucket = "sec-app-terraform-backend"
    key    = "terraform.tfstate"
    region = "us-east-2"
  }
}

provider "aws" {
  region = "us-east-2"
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.21.0"

  name = var.vpc_name
  cidr = var.vpc_cidr

  azs             = var.vpc_azs
  private_subnets = var.vpc_private_subnets

  tags = var.vpc_tags

  enable_dns_hostnames = true
  enable_dns_support   = true
}

data "aws_acm_certificate" "andrewkreuzer" {
  domain   = "andrewkreuzer.ca"
  statuses = ["ISSUED"]
}

module "api_gateway" {
  source = "terraform-aws-modules/apigateway-v2/aws"

  name          = "sec-app-api-gateway"
  description   = "API gateway for our secure web app"
  protocol_type = "HTTP"

  domain_name                 = "app.andrewkreuzer.ca"
  domain_name_certificate_arn = data.aws_acm_certificate.andrewkreuzer.arn

  subnet_ids      = module.vpc.private_subnets
  create_vpc_link = true
}

resource "aws_apigatewayv2_route" "api_route" {
  api_id    = module.api_gateway.this_apigatewayv2_api_id
  route_key = "$default"
  target    = "integrations/${aws_apigatewayv2_integration.api_integration.id}"
}

resource "aws_apigatewayv2_integration" "api_integration" {
  api_id             = module.api_gateway.this_apigatewayv2_api_id
  integration_type   = "HTTP_PROXY"
  integration_method = "GET"
  connection_type    = "VPC_LINK"
  connection_id      = module.api_gateway.this_apigatewayv2_vpc_link_id
  integration_uri    = module.alb.http_tcp_listener_arns[0]
}

module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 5.0"

  name = "sec-app-alb"

  load_balancer_type = "application"
  internal           = true
  vpc_id             = module.vpc.vpc_id
  security_groups    = [module.vpc.default_security_group_id]
  subnets            = module.vpc.private_subnets

  target_groups = [
    {
      target_type      = "ip"
      backend_port     = 80
      backend_protocol = "HTTP"
      vpc_id           = module.vpc.vpc_id
    },
    {
      target_type      = "ip"
      backend_port     = 80
      backend_protocol = "HTTP"
      vpc_id           = module.vpc.vpc_id
    },
  ]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    },
  ]
}

resource "aws_security_group" "ecr_endpoint_sg" {
  name        = "ecr_endpoint_sg"
  description = "Allow TLS inbound traffic to the vpc endpoints"
  vpc_id      = module.vpc.vpc_id

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = module.vpc.private_subnets_cidr_blocks
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id          = module.vpc.vpc_id
  service_name    = "com.amazonaws.us-east-2.s3"
  route_table_ids = module.vpc.private_route_table_ids
}

resource "aws_vpc_endpoint" "ecr_api" {
  vpc_id              = module.vpc.vpc_id
  service_name        = "com.amazonaws.us-east-2.ecr.api"
  vpc_endpoint_type   = "Interface"
  subnet_ids          = module.vpc.private_subnets
  private_dns_enabled = true

  security_group_ids = [aws_security_group.ecr_endpoint_sg.id]
}

resource "aws_vpc_endpoint" "ecr_dkr" {
  vpc_id              = module.vpc.vpc_id
  service_name        = "com.amazonaws.us-east-2.ecr.dkr"
  vpc_endpoint_type   = "Interface"
  subnet_ids          = module.vpc.private_subnets
  private_dns_enabled = true

  security_group_ids = [aws_security_group.ecr_endpoint_sg.id]
}

resource "aws_vpc_endpoint" "logs" {
  vpc_id              = module.vpc.vpc_id
  service_name        = "com.amazonaws.us-east-2.logs"
  vpc_endpoint_type   = "Interface"
  subnet_ids          = module.vpc.private_subnets
  private_dns_enabled = true

  security_group_ids = [aws_security_group.ecr_endpoint_sg.id]
}

module "ecs" {
  source = "./modules/fargate_ecs"

  vpc_subnets                = module.vpc.private_subnets
  ecs_alb_target_group       = module.alb.target_group_arns
  ecs_alb_target_group_names = module.alb.target_group_names
  ecs_alb_prod_listeners     = module.alb.http_tcp_listener_arns
  vpc_id                     = module.vpc.vpc_id
  vpc_cidr_block             = var.vpc_cidr
  s3_endpoint_prefix_list_id = aws_vpc_endpoint.s3.prefix_list_id
}

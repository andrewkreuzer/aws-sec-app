provider "aws" {
  region = "us-east-1"
  alias = "east1"
}

module "cert-east-1" {
  source = "./modules/cert"
  providers = {
    aws = aws.east1
  }
}

provider "aws" {
  region = "us-east-2"
  alias = "east2"
}

module "cert-east-2" {
  source = "./modules/cert"
  providers = {
    aws = aws.east2
  }
}

module "cognito" {
  source = "./modules/cognito"
  providers = {
    aws = aws.east1
  }

  cert = module.cert-east-1.arn
  google_oauth_client_id = var.google_oauth_client_id
  google_oauth_client_secret = var.google_oauth_client_secret
}

module "atlantis" {
  source = "./modules/atlantis"
  providers = {
    aws = aws.east1
  }

  cert = module.cert-east-1.arn

  gitlab_user = var.gitlab_user
  gitlab_token = var.gitlab_token

  gitlab_allowed_repo_names = var.gitlab_allowed_repo_names

  google_oauth_client_id = var.google_oauth_client_id
  google_oauth_client_secret = var.google_oauth_client_secret

  user_pool_arn = module.cognito.user_pool_arn
  user_pool_client_id = module.cognito.user_pool_client_id
  user_pool_domain = module.cognito.user_pool_domain
}

module "gitlab" {
  source = "./modules/gitlab"

  gitlab_token = var.gitlab_token

  atlantis_allowed_repo_names = var.atlantis_allowed_repo_names
  webhook_url = module.atlantis.atlantis_url_events
  webhook_secret = module.atlantis.webhook_secret
}

module "ecr" {
  source = "./modules/ecr"
  providers = {
    aws = aws.east2
  }
}

variable "gitlab_user" {
  type = string
}

variable "gitlab_token" {
  type = string
}

variable "gitlab_allowed_repo_names" {
  type = list(string)
}

variable "atlantis_allowed_repo_names" {
  type = list(string)
}

variable "google_oauth_client_id" {
  type = string
}

variable "google_oauth_client_secret" {
  type = string
}

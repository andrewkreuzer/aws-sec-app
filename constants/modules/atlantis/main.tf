module "atlantis" {
  source  = "terraform-aws-modules/atlantis/aws"
  version = "~> 2.0"

  name = "atlantis"

  # VPC
  cidr            = "10.10.0.0/16"
  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets = ["10.10.1.0/24", "10.10.2.0/24", "10.10.3.0/24"]
  public_subnets  = ["10.10.101.0/24", "10.10.102.0/24", "10.10.103.0/24"]

  ecs_fargate_spot = true

  # DNS (without trailing dot)
  route53_zone_name   = "andrewkreuzer.ca"
  route53_record_name = "atlantis.andrewkreuzer.ca"

  certificate_arn = var.cert

  # Atlantis
  atlantis_gitlab_user       = var.gitlab_user
  atlantis_gitlab_user_token = var.gitlab_token
  atlantis_repo_whitelist    = var.gitlab_allowed_repo_names

  alb_authenticate_cognito = {
    user_pool_arn       = var.user_pool_arn
    user_pool_client_id = var.user_pool_client_id
    user_pool_domain    = var.user_pool_domain
  }

  allow_unauthenticated_access = true
  allow_github_webhooks        = true
}

variable "gitlab_user" {
  type = string
}

variable "gitlab_token" {
  type = string
}

variable "gitlab_allowed_repo_names" {
  type = list(string)
}

variable "cert" {
  type = string
}

variable "google_oauth_client_id" {
  type = string
}

variable "google_oauth_client_secret" {
  type = string
}

variable "user_pool_arn" {
  type = string
}

variable "user_pool_client_id" {
  type = string
}

variable "user_pool_domain" {
  type = string
}

output "atlantis_url_events" {
  value = module.atlantis.atlantis_url_events
}

output "webhook_secret" {
  value = module.atlantis.webhook_secret
}

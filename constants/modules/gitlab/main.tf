provider "gitlab" {
  token    = var.gitlab_token
}

resource "gitlab_project_hook" "this" {

  count = length(var.atlantis_allowed_repo_names)

  project                 = var.atlantis_allowed_repo_names[count.index]
  url                     = var.webhook_url
  token                   = var.webhook_secret
  enable_ssl_verification = false

  merge_requests_events = true
  push_events           = true
  note_events           = true
}

variable "gitlab_token" {
  type = string
}

variable "webhook_url" {
  type = string
}

variable "webhook_secret" {
  type = string
}

variable "atlantis_allowed_repo_names" {
  type = list(string)
}

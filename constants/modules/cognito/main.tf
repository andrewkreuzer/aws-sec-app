resource "aws_cognito_user_pool" "atlantis-signin" {
  name = "atlantis-signin"

  username_attributes = ["email"]
  password_policy {
    minimum_length = 12
    require_lowercase = true
    require_numbers = true
    require_symbols = true
    require_uppercase =true
    temporary_password_validity_days = 1
  }

  mfa_configuration = "OPTIONAL"
  software_token_mfa_configuration {
    enabled = true
  }

  admin_create_user_config {
    allow_admin_create_user_only = true
  }

  account_recovery_setting {
    recovery_mechanism {
      name     = "admin_only"
      priority = 1
    }
  }

  verification_message_template {
    default_email_option = "CONFIRM_WITH_CODE"
    email_subject = "Account Confirmation"
    email_message = "Your confirmation code is {####}"
  }

  device_configuration {
    challenge_required_on_new_device = true
    device_only_remembered_on_user_prompt = true
  }
}

resource "aws_cognito_identity_provider" "atlantis-google-provider" {
  user_pool_id  = aws_cognito_user_pool.atlantis-signin.id
  provider_name = "Google"
  provider_type = "Google"

  provider_details = {
    authorize_scopes = "profile email openid"
    client_id        = var.google_oauth_client_id
    client_secret    = var.google_oauth_client_secret
  }

  attribute_mapping = {
    email    = "email"
    username = "sub"
  }
}

resource "aws_cognito_user_pool_client" "atlantis-client" {
  name = "atlantis-client"
  depends_on = [aws_cognito_identity_provider.atlantis-google-provider]

  user_pool_id = aws_cognito_user_pool.atlantis-signin.id
  generate_secret = true
  refresh_token_validity = 90
  prevent_user_existence_errors = "ENABLED"
  explicit_auth_flows = [
    "ALLOW_REFRESH_TOKEN_AUTH",
    "ALLOW_USER_PASSWORD_AUTH",
    "ALLOW_ADMIN_USER_PASSWORD_AUTH",
    "ALLOW_USER_SRP_AUTH",
  ]

  allowed_oauth_flows_user_pool_client = true
  allowed_oauth_flows = ["code"]
  allowed_oauth_scopes = ["profile", "email", "openid"]
  callback_urls = [
    "https://atlantis.andrewkreuzer.ca/oauth2/idpresponse",
    "https://auth.andrewkreuzer.ca/oauth2/idpresponse"
  ]

  supported_identity_providers = ["Google", "COGNITO"]
}

data "aws_route53_zone" "andrewkreuzer" {
  name = "andrewkreuzer.ca"
}

resource "aws_cognito_user_pool_domain" "atlantis" {
  domain          = "auth.andrewkreuzer.ca"
  certificate_arn = var.cert
  user_pool_id    = aws_cognito_user_pool.atlantis-signin.id
}

resource "aws_route53_record" "atlantis-auth-A" {
  name    = aws_cognito_user_pool_domain.atlantis.domain
  type    = "A"
  zone_id = data.aws_route53_zone.andrewkreuzer.zone_id
  alias {
    evaluate_target_health = false
    name                   = aws_cognito_user_pool_domain.atlantis.cloudfront_distribution_arn
    zone_id = "Z2FDTNDATAQYW2"
  }
}

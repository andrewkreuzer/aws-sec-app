resource "aws_ecr_repository" "sec-app" {
  name = "sec-app"

  image_scanning_configuration {
    scan_on_push = true
  }
}
